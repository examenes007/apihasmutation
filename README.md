# ApiHasMutation

## Getting started

To start the project 

Download from 

```
git clone git@gitlab.com:examenes007/apihasmutation.git
```
If it’s the first time type in the command line

```
npm install
```
and then to run

```
npm index.js
```

## Test and Deploy

Preconditions: For testing it's required to install in mode developer the modules jest and supertest

```
npm install --save-dev jest
npm install --save-dev supertest
```

Testing run:

```
npm test
```

Expected output: (Warning execute test clean the database for consistence test)

```
> apihasmutation@1.0.0 test
> jest

 PASS  tests/index.spec.js (5.729 s)
  Unit test method hasMutation
    ✓ should return true if the element has a mutation (3 ms)
    ✓ should return false if the element hasn't a mutation
    ✓ should return null if the element has a dnaInvalid (1 ms)
  Preparation for integral test
    ✓ should clean database (1199 ms)
  Integral test of the services
    ✓ should return 200 if alive (62 ms)
    ✓ should return 200 if the element has a mutation (881 ms)
    ✓ should return 403 if the element hasn't a mutation (845 ms)
    ✓ should return 500 if the element invalid secuence (19 ms)
    ✓ should return 500 if the element invalid size (9 ms)
    ✓ should return 500 the mutation exist in database (870 ms)
    ✓ should return a json with the ratio of mutation (931 ms)

Test Suites: 1 passed, 1 total
Tests:       11 passed, 11 total
Snapshots:   0 total
Time:        5.831 s, estimated 16 s
Ran all test suites.
```

¿Why I chose this FrameWorks?

Source of analisis
https://2020.stateofjs.com/en-US/technologies/testing/


JEST
https://jestjs.io/


Supertest (it allow me to test Rest without deploy)
https://github.com/visionmedia/supertest


Deploy

    -   Seccion in constrution

## Name
ApiHasMutation

## Description
ApiHasMutation is an exercise for demonstrating the acknowledgment of

    -	Basic algorithms and validations
    -	Use of service Rest (get/post)
    -	Access to the database (in this case with MongoDB)
    -	Use of status codes
    -	Use of javascript language
    -	Use of nodejs Frame Work
    -	JSON manipulation
    -	Unit testing and integral (using jest in this case)
    -	Use of GIT
    -	Deploy on cloud
    -	And patience :)


## Installation

Download the code from

```
git clone git@gitlab.com:examenes007/apihasmutation.git
cd apihasmutation
npm install
npm index.js
```

## Develop Usage

For develop process user develop branch

```
git checkout develop
```

For production make your merge request to main from your baranch
```
git checkout main
git merge develop
git add .
git commit -m "Describe the new Feature, Issue or Bug Fix"
git push origin main
```

## Usage and examples

Check if aplication is active

```
 curl http://localhost:3000/status

```

Review ratio

```
 curl http://localhost:3000/stats
```

DNA has mutation (return code 200)

```
curl --location --request POST 'http://localhost:3000/mutation' \
--header 'Content-Type: application/json' \
--data-raw '{ "dna": ["ATGCGA", "CAGTGC", "TTATGT", "AGAAGG", "CCCCTA", "TCACTG"] }'
```

DNA hasn't a mutation  (return code 403)

```
curl --location --request POST 'http://localhost:3000/mutation' \
--header 'Content-Type: application/json' \
--data-raw '{ "dna": ["ATGCGA", "CAGTGC", "TTATTT", "AGACGG", "GCGTCA", "TCACTG"] }'

```

Try invalid verification  (return code 500)
```
curl --location --request POST 'http://localhost:3000/mutation' \
--header 'Content-Type: application/json' \
--data-raw '{ "dna": ["ATGCGA", "CAGTGC", "TTATTT", "AGACGG", "GCGTCA", "TCACTX"] }'
```

Try invalid size  (return code 500)
```
curl --location --request POST 'http://localhost:3000/mutation' \
--header 'Content-Type: application/json' \
--data-raw '{ "dna": ["ATGCGAA", "CAGTGC", "TTATTT", "AGACGG", "GCGTCA", "TCACTX"] }'
```

Try repeated verification  (return code 500)
```
curl --location --request POST 'http://localhost:3000/mutation' \
--header 'Content-Type: application/json' \
--data-raw '{ "dna": ["ATGCGA", "CAGTGC", "TTATTT", "AGACGG", "GCGTCA", "TCACTG"] }'
```

## Support
For any questions please send mail to hector_durante@gmail.com

## Contributing
None

## Authors and acknowledgment
Business user: Andres Szwarcfite
Developer: Hector Manuel Durante Nuñez

## License
MIT License

## Project status
In revision

