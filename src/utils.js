const { MongoClient, ServerApiVersion } = require('mongodb');
//TODO: add parametized credentials, credential expire in 6 days THIS IS ONLY FOR TEST PURPOSES
let userMongo = 'UsrTeamknowlogy';
let passwordMongo = 'm6nERlJkGWTqe2dU7H'; 

/** 
 * Delete all the documents in the collection for testing purposes
*/
async function delete_all_adn() {   
    const uri = "mongodb+srv://" + userMongo + ":" + passwordMongo + "@mongodev.pv3ra.mongodb.net/teamknowlogy?retryWrites=true&w=majority";
    let client = new MongoClient(uri, { useNewUrlParser: true, useUnifiedTopology: true, serverApi: ServerApiVersion.v1 });
    let cuantos=0
    try {        
        await client.connect();
        let database = client.db("teamknowlogy");
        let collection = database.collection("dna");
        cuantos =  (await collection.deleteMany({})).deletedCount;               
    }
    finally {
        client.close();        
    }      
    return cuantos 
}



module.exports = {delete_all_adn}

