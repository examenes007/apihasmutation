
/**
 * Module to access the database 
 */
//TODO: add parametized credentials, credential expire in 6 days THIS IS ONLY FOR TEST PURPOSES
let userMongo = 'UsrTeamknowlogy';
let passwordMongo = 'm6nERlJkGWTqe2dU7H';
//----------------------------------------------------------------------------------------------------------------------
const { MongoClient, ServerApiVersion } = require('mongodb');
const uri = "mongodb+srv://" + userMongo + ":" + passwordMongo + "@mongodev.pv3ra.mongodb.net/teamknowlogy?retryWrites=true&w=majority";


/**
 * 
 * @param {*} dna 
 * @returns id of the element inserted in monogoDB
 */
async function guardar_dna(dna) {
    let client = new MongoClient(uri, { useNewUrlParser: true, useUnifiedTopology: true, serverApi: ServerApiVersion.v1 });
    let result
    try {
        await client.connect();
        database = client.db("teamknowlogy");
        collection = database.collection("dna");
        result = await collection.insertOne(dna);

    }
    finally {
        client.close();
    }
    return result.insertedId
}

/**
 * 
 * @returns the ratio of the number of mutations in the database
 */
async function estadisticas_adn() {
    const uri = "mongodb+srv://" + userMongo + ":" + passwordMongo + "@mongodev.pv3ra.mongodb.net/teamknowlogy?retryWrites=true&w=majority";
    let client = new MongoClient(uri, { useNewUrlParser: true, useUnifiedTopology: true, serverApi: ServerApiVersion.v1 });
    let count_mutations = 0
    let count_no_mutation = 0
    let ratio = 0.0
    try {
        await client.connect();
        let database = client.db("teamknowlogy");
        let collection = database.collection("dna");
        count_mutations = await collection.countDocuments({ mutacion: true });
        count_no_mutation = await collection.countDocuments({ mutacion: false });
        ratio = parseFloat(count_mutations) / parseFloat(count_mutations + count_no_mutation)        
    }
    finally {
        client.close();
    }
    return JSON.stringify({ "count_mutations": count_mutations, "count_no_mutation": count_no_mutation, "ratio": ratio })
}


module.exports = { estadisticas_adn, guardar_dna }