/**
 * Service rest to process dna and locate mutations
 */

const mutation = require("./mutation.js");
const mongo = require("./mongodb.js");
const express = require("express");
const app = express();
app.use(express.json());
const cors = require('cors');
app.use(cors({
    origin: '*' //TODO ADVERTENCIA se desactivo para que funcione en todos los dominios durante las pruebas
}));


/**
 * @returns Ok to verify if the service it's alive
 */
app.get('/status', function (req, res) {
    res.status(200).send('OK')
});


/**
 * @returns the ratio of the number of mutations in the database
 */
app.get('/stats', async function (req, res) {
    var estadistica = await mongo.estadisticas_adn()
    res.status(200).send(estadistica)
});

/**
 * @param {string} dna
 * @returns 200.ok if the dna is valid
 * @returns 403.forbiden if the dna is not valid
 * @returns 500.Internal Server Error for any other error
 */
app.post('/mutation', async function (req, res) {
    try {
        if (req.body.hasOwnProperty('dna')) {
            let resultado = mutation.hasMutation(req.body.dna)
            if (resultado == null) {
                res.status(400).send('Bad Request')
            } else if (resultado) {
                await mongo.guardar_dna(JSON.parse(JSON.stringify({ 'dna': req.body.dna.join(" "), "mutacion": resultado })))
                res.status(200).send('OK')
            }
            else {
                await mongo.guardar_dna(JSON.parse(JSON.stringify({ 'dna': req.body.dna.join(" "), "mutacion": resultado })))
                res.status(403).send('Forbidden')
            }
        }
    } catch (e) {
        res.status(500).send('Internal Server Error')
    }
});


module.exports = { app };
