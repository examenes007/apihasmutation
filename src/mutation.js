/**
 * 
 * @param {*} dna 
 * @returns false if the element hasn't a mutation
 * @returns true if the element has a mutation
 * @returns null if the element has a dnaInvalid
 */
function hasMutation(dna) {
    let tamano = dna.length - 1;
    let vertical = ""
    let diagonalizq = ""
    let diagonalder = ""
    const tamanoMatrizPorDiseno=6 // TODO: Mejorar esto para que sea paarmetrizable o poner un modulo de constantes

    if(tamano != tamanoMatrizPorDiseno-1) throw new Error('Matrix invalid size')

    //invalid chars
    for (let i = 0; i <= tamano; i++) {
        if(dna[i].length != tamanoMatrizPorDiseno) throw new Error('Matrix invalid size')
        
        for (let j = 0; j <= tamano; j++) {
            c = dna[j][i]
            if (c == 'A' || c == 'C' || c == 'G' || c == 'T') { vertical += c }
            else { throw new Error('Invalid value') }
        }
        vertical += " "
        diagonalizq += dna[i][i]
        diagonalder += dna[i][tamano - i]
    }
    let salida = dna.join(" ") + " " + vertical + " " + diagonalizq + " " + diagonalder

    if (salida.includes('AAAA') || salida.includes('CCCC') || salida.includes('GGGG') || salida.includes('TTTT')) {
        return true
    }
    else {
        return false
    }

}


module.exports = { hasMutation }