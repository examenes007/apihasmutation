/**
 * Unit & Integral tests for the hasMutaion method's
 */

const mutation = require("../src/mutation.js");
const app = require("../src/app.js");
const request = require("supertest");
const { response } = require("express");
const utils = require("../src/utils.js");


//unit test for hasMutation
var dnaMutation = {
    "dna": ["ATGCGA", "CAGTGC", "TTATGT", "AGAAGG", "CCCCTA", "TCACTA"]
}

var dna = {
    "dna": ["ATGCGA", "CAGTGC", "TTATTT", "AGACGG", "GCGTCA", "TCACTA"]
}

var dnaInvalid = {
    "dna": ["ATGCGA", "CAGTGC", "TTATTT", "AGACGG", "GCGTCA", "TCACTX"]
}


describe("Unit test method hasMutation", () => {

    test("should return true if the element has a mutation", () => {
        expect(mutation.hasMutation(dnaMutation.dna)).toBe(true);
    })

    test("should return false if the element hasn't a mutation", () => {
        expect(mutation.hasMutation(dna.dna)).toBe(false);
    })

    test("should return null if the element has a dnaInvalid", () => {
        try{
            expect(mutation.hasMutation(dnaInvalid.dna)).toBe(null);    
        } catch {
            expect(true).toBe(true);
        }
        
    })    

})


// Function to ensure repetition of integration tests BE CAREFUL IT'S CLEAN THE DATABASE ONLY FOR INTEGRATION TESTS
describe("Preparation for integral test", () => {
    test("should clean database", async () => {
        expect(await utils.delete_all_adn()).toBeGreaterThanOrEqual(0) //Solo ambienta la bd para que las pruebas sean consistentes y repetitivas
    })
})

// Integration test for the service rest
describe("Integral test of the services", () => {
    test("should return 200 if alive", async () => {
        const response = await request(app.app).get("/status").send();
        expect(response.status).toBe(200);
    })
    test("should return 200 if the element has a mutation", async () => {
        const response = await request(app.app).post("/mutation").send({ "dna": ["ATGCGA", "CAGTGC", "TTATGT", "AGAAGG", "CCCCTA", "TCACTG"] });
        expect(response.status).toBe(200)
    })

    test("should return 403 if the element hasn't a mutation", async () => {
        const response = await request(app.app).post("/mutation").send({ "dna": ["ATGCGA", "CAGTGC", "TTATTT", "AGACGG", "GCGTCA", "TCACTG"] });
        expect(response.status).toBe(403)
    })

    test("should return 500 if the element invalid secuence", async () => {
        const response = await request(app.app).post("/mutation").send({ "dna": ["ATGCGA", "CAGTGC", "TTATTT", "AGACGG", "GCGTCA", "TCACTX"] });
        expect(response.status).toBe(500)
    })

    test("should return 500 if the element invalid size", async () => {
        const response = await request(app.app).post("/mutation").send({ "dna": ["ATGCGAA", "CAGTGC", "TTATTT", "AGACGG", "GCGTCA", "TCACTX"] });
        expect(response.status).toBe(500)
    })

    test("should return 500 the mutation exist in database", async () => {
        const response = await request(app.app).post("/mutation").send({ "dna": ["ATGCGA", "CAGTGC", "TTATGT", "AGAAGG", "CCCCTA", "TCACTG"] });
        expect(response.status).toBe(500) 
    })
    
    test("should return a json with the ratio of mutation", async () => {
        const response = await request(app.app).get("/stats").send();        
        expect(response.status).toBe(200)
    })
})

